import readline from "readline";
import createMemory from "../src/createMemory";
import CPU from "../src/cpu";
import { instructions } from "../src/instructions";
import { registers } from "../src/registers";

const memory = createMemory(256 * 256);
const writeableBytes = new Uint8Array(memory.buffer);
const cpu = new CPU(memory);

let i = 0;
writeableBytes[i++] = instructions.MOV_LIT_REG;
writeableBytes[i++] = 0x00;
writeableBytes[i++] = 0x01;
writeableBytes[i++] = registers.R1;

writeableBytes[i++] = instructions.ADD_REG_REG;
writeableBytes[i++] = registers.R1;
writeableBytes[i++] = registers.ACC;

writeableBytes[i++] = instructions.JNE_LIT;
writeableBytes[i++] = 0x00;
writeableBytes[i++] = 0x03;
writeableBytes[i++] = 0x00;
writeableBytes[i++] = 0x04;

writeableBytes[i++] = instructions.MOV_REG_MEM;
writeableBytes[i++] = registers.ACC;
writeableBytes[i++] = 0x01;
writeableBytes[i++] = 0x00;

writeableBytes[i++] = instructions.HLT;

const readlineInterface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

cpu.debug();

readlineInterface.on("line", () => {
  cpu.step();
  cpu.debug();
  cpu.viewMemoryAt(0x100);
});
