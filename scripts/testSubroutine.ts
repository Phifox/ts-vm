import readline from "readline";
import createMemory from "../src/createMemory";
import CPU from "../src/cpu";
import { instructions } from "../src/instructions";
import { registers } from "../src/registers";

const memory = createMemory(256 * 256);
const writeableBytes = new Uint8Array(memory.buffer);
const cpu = new CPU(memory);

let i = 0;
writeableBytes[i++] = instructions.MOV_LIT_REG;
writeableBytes[i++] = 0x13;
writeableBytes[i++] = 0x37;
writeableBytes[i++] = registers.R1;

writeableBytes[i++] = instructions.MOV_LIT_REG;
writeableBytes[i++] = 0xac;
writeableBytes[i++] = 0xab;
writeableBytes[i++] = registers.R2;

writeableBytes[i++] = instructions.PSH_LIT;
writeableBytes[i++] = 0xfe;
writeableBytes[i++] = 0xfe;

writeableBytes[i++] = instructions.PSH_LIT;
writeableBytes[i++] = 0xba;
writeableBytes[i++] = 0xba;

// push the numbers of arguments for the subroutine, e.g. 0
writeableBytes[i++] = instructions.PSH_LIT;
writeableBytes[i++] = 0x00;
writeableBytes[i++] = 0x00;

writeableBytes[i++] = instructions.CAL_LIT;
writeableBytes[i++] = 0x01;
writeableBytes[i++] = 0x00;

writeableBytes[i++] = instructions.MOV_REG_REG;
writeableBytes[i++] = registers.ACC;
writeableBytes[i++] = registers.R3;

writeableBytes[i++] = instructions.PSH_LIT;
writeableBytes[i++] = 0x12;
writeableBytes[i++] = 0x34;

writeableBytes[i++] = instructions.POP;
writeableBytes[i++] = registers.R1;

writeableBytes[i++] = instructions.POP;
writeableBytes[i++] = registers.R2;

writeableBytes[i++] = instructions.HLT;

//SubRoutine:
let j = 0x100;

writeableBytes[j++] = instructions.ADD_REG_REG;
writeableBytes[j++] = registers.R1;
writeableBytes[j++] = registers.R2;

writeableBytes[j++] = instructions.PSH_LIT;
writeableBytes[j++] = 0x00;
writeableBytes[j++] = 0x42;

writeableBytes[j++] = instructions.PSH_LIT;
writeableBytes[j++] = 0x31;
writeableBytes[j++] = 0x41;

writeableBytes[j++] = instructions.RET;

const readlineInterface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

cpu.debug();

// readlineInterface.on("line", () => {
//   cpu.step();
//   cpu.debug();
//   cpu.viewMemoryAt(0xfffe - 42, 44);
// });

cpu.runWithDebug();
