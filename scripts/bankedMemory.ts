import readline from "readline";
import createMemory from "../src/createMemory";
import CPU from "../src/cpu";
import MemoryMapper from "../src/memoryMapper";
import compile from "../assembler";
import { registers } from "../src/registers";
import { register } from "../assembler/parser/primitives";

const MM = new MemoryMapper();

const dataViewMethods = ["getUint8", "getUint16", "setUint8", "setUint16"];

function createBankedMemory(n: number, bankSize: number, cpu: CPU) {
  const bankBuffers = Array.from(
    { length: n },
    () => new ArrayBuffer(bankSize)
  );
  const banks = bankBuffers.map((buffer) => new DataView(buffer));

  const forwardToDataView =
    (name) =>
    (...args) => {
      const bankIndex = cpu.getRegister(registers.MB) % n;
      return banks[bankIndex][name](...args);
    };

  const dataViewInterface = dataViewMethods.reduce((dataView, fnName) => {
    dataView[fnName] = forwardToDataView(fnName);
    return dataView;
  }, {});

  return dataViewInterface as DataView;
}

const bankSize = 0xff;
const nBanks = 8;
const cpu = new CPU(MM);

const memoryBankDevice = createBankedMemory(nBanks, bankSize, cpu);
MM.map(memoryBankDevice, 0, bankSize);

const regularMemory = createMemory(0xff00);
MM.map(regularMemory, bankSize, 0xffff, true);

console.log("writing value 1 to address 0");
MM.setUint16(0, 1);
console.log(MM.getUint16(0));

console.log("writing value 42 to address 0xee (bank 7)");

cpu.setRegister(registers.MB, 7);

MM.setUint16(0xee, 42);
console.log(MM.getUint16(0));
console.log(MM.getUint16(0xee));

cpu.setRegister(registers.MB, 0);

console.log(MM.getUint16(0));
console.log(MM.getUint16(0xee));
