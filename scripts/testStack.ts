import readline from "readline";
import createMemory from "../src/createMemory";
import CPU from "../src/cpu";
import { instructions } from "../src/instructions";
import { registers } from "../src/registers";

const memory = createMemory(256 * 256);
const writeableBytes = new Uint8Array(memory.buffer);
const cpu = new CPU(memory);

let i = 0;
writeableBytes[i++] = instructions.MOV_LIT_REG;
writeableBytes[i++] = 0x13;
writeableBytes[i++] = 0x37;
writeableBytes[i++] = registers.R1;

writeableBytes[i++] = instructions.MOV_LIT_REG;
writeableBytes[i++] = 0xac;
writeableBytes[i++] = 0xab;
writeableBytes[i++] = registers.R2;

writeableBytes[i++] = instructions.PSH_REG;
writeableBytes[i++] = registers.R1;

writeableBytes[i++] = instructions.PSH_REG;
writeableBytes[i++] = registers.R2;

writeableBytes[i++] = instructions.PSH_LIT;
writeableBytes[i++] = 0xfe;
writeableBytes[i++] = 0xfe;

writeableBytes[i++] = instructions.POP;
writeableBytes[i++] = registers.R3;

writeableBytes[i++] = instructions.POP;
writeableBytes[i++] = registers.R1;

writeableBytes[i++] = instructions.POP;
writeableBytes[i++] = registers.R2;

writeableBytes[i++] = instructions.HLT;

const readlineInterface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

cpu.debug();

readlineInterface.on("line", () => {
  cpu.step();
  cpu.debug();
});
