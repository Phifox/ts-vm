import readline from "readline";
import createMemory from "../src/createMemory";
import CPU from "../src/cpu";
import { instructions } from "../src/instructions";
import MemoryMapper from "../src/memoryMapper";
import ScreenDevice from "../src/ScreenDevice";

const MM = new MemoryMapper();

const memory = createMemory(256 * 256);
MM.map(memory, 0x0000, 0xffff);

const screen = new ScreenDevice();
MM.map(screen, 0x3000, 0x30ff, true);

const writeableBytes = new Uint8Array(memory.buffer);
const cpu = new CPU(MM);

let i = 0;

function print(text, offset = 0x0000) {
  let position = 0x3000 + offset;

  text.split("").forEach((char) => {
    if (char === "\n") {
      position = (position & 0xfff0) + 0x0010;
      return;
    }

    writeableBytes[i++] = instructions.MOV_LIT_MEM;
    writeableBytes[i++] = 0x00;
    writeableBytes[i++] = char.charCodeAt(0);
    writeableBytes[i++] = (position & 0xff00) >> 8;
    writeableBytes[i++] = position & 0x00ff;
    position++;
  });
}

print("Hello World! \nGood to see you!");

writeableBytes[i++] = instructions.HLT;

console.clear();
cpu.run();

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "H".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x00;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "e".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x01;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "l".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x02;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "l".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x03;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "o".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x04;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = " ".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x05;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "W".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x06;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "o".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x07;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "r".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x08;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "l".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x09;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "d".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x0a;

// writeableBytes[i++] = instructions.MOV_LIT_MEM;
// writeableBytes[i++] = 0x00;
// writeableBytes[i++] = "!".charCodeAt(0);
// writeableBytes[i++] = 0x30;
// writeableBytes[i++] = 0x0b;
