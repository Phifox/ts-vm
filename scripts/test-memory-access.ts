import createMemory from "../src/createMemory";
import CPU from "../src/cpu";
import { instructions } from "../src/instructions";
import { registers } from "../src/registers";

const memory = createMemory(256 * 256);
const writeableBytes = new Uint8Array(memory.buffer);
const cpu = new CPU(memory);

let i = 0;
writeableBytes[i++] = instructions.MOV_LIT_REG;
writeableBytes[i++] = 0x12;
writeableBytes[i++] = 0x34;
writeableBytes[i++] = registers.R1;

writeableBytes[i++] = instructions.MOV_LIT_REG;
writeableBytes[i++] = 0xab;
writeableBytes[i++] = 0xcd;
writeableBytes[i++] = registers.R2;

writeableBytes[i++] = instructions.MOV_REG_MEM;
writeableBytes[i++] = registers.R1;
writeableBytes[i++] = 0x01;
writeableBytes[i++] = 0x00;

writeableBytes[i++] = instructions.MOV_REG_REG;
writeableBytes[i++] = registers.R2;
writeableBytes[i++] = registers.R1;

writeableBytes[i++] = instructions.MOV_MEM_REG;
writeableBytes[i++] = 0x01;
writeableBytes[i++] = 0x00;
writeableBytes[i++] = registers.R2;

writeableBytes[i++] = instructions.HLT;

cpu.debug();

cpu.run();
