import readline from "readline";
import createMemory from "../src/createMemory";
import CPU from "../src/cpu";
import compile from "../assembler";

const memory = createMemory(256 * 256);
const cpu = new CPU(memory);

const assemblerProgram = `
start:
  mov $0A, &0050
loop:
  mov &0050 , acc
  dec acc
  mov acc, &0050
  inc r2
  inc r2
  inc r2
  jne $00, &[!loop]
end:
  hlt
`;

const machineCode = compile(assemblerProgram);
cpu.load(machineCode);

const readlineInterface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

cpu.debug();

readlineInterface.on("line", () => {
  cpu.step();
  cpu.debug();
});
