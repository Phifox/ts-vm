import readline from "readline";
import createMemory from "../src/createMemory";
import CPU from "../src/cpu";
import compile from "../assembler";

const memory = createMemory(256 * 256);
const cpu = new CPU(memory);

const assemblerProgram = `
mov $C0DE , r1
mov $1DEA , r2
add r1, r2
mov acc , &0100
hlt
`;

const machineCode = compile(assemblerProgram);
cpu.load(machineCode);

const readlineInterface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

cpu.debug();

readlineInterface.on("line", () => {
  cpu.step();
  cpu.debug();
});
