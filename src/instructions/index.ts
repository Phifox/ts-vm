import { meta } from "./meta";

interface Instruction {
  instruction: string;
  opcode: number;
  size: number;
  type: string;
}

interface Instructions {
  [instruction: string]: Instruction;
}

const instructions: Instructions = meta.reduce((instructions, instruction) => {
  instructions[instruction.instruction] = instruction;
  return instructions;
}, {});

export default instructions;
