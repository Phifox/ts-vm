import { Memory } from "./createMemory";

interface Region {
  device: Memory;
  start: number;
  end: number;
  remap: boolean;
}

export default class MemoryMapper {
  regions: Region[];
  byteLength: number;
  buffer: ArrayBuffer;

  constructor() {
    this.regions = [];
    this.byteLength = 0xffff;
    this.buffer = new ArrayBuffer(0xffff);
  }

  map(
    device: Memory,
    start: number,
    end: number,
    remap: boolean = true
  ): () => void {
    const region = {
      device,
      start,
      end,
      remap,
    };

    this.regions.unshift(region);

    return () => {
      this.regions = this.regions.filter((item) => item !== region);
    };
  }

  findRegion(address): Region {
    const region = this.regions.find(
      (region) => region.start <= address && address <= region.end
    );
    if (!region) {
      throw new Error(
        `MemoryMapper.findRegion: No region found at address 0x${address.toString(
          16
        )}.`
      );
    }

    return region;
  }

  getUint16(address: number): number {
    const region = this.findRegion(address);
    const finalAddress = region.remap ? address - region.start : address;

    return region.device.getUint16(finalAddress);
  }

  getUint8(address: number): number {
    const region = this.findRegion(address);
    const finalAddress = region.remap ? address - region.start : address;

    return region.device.getUint8(finalAddress);
  }

  setUint16(address: number, value: number): void {
    const region = this.findRegion(address);
    const finalAddress = region.remap ? address - region.start : address;

    region.device.setUint16(finalAddress, value);
  }

  setUint8(address: number, value: number): void {
    const region = this.findRegion(address);
    const finalAddress = region.remap ? address - region.start : address;

    region.device.setUint8(finalAddress, value);
  }
}
