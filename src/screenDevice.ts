function moveTo(x: number, y: number): void {
  process.stdout.write(`\x1b[${y};${2 * x}H`);
}

function print(charCode: number): void {
  const character = String.fromCharCode(charCode);
  process.stdout.write(character);
}

function clearScreen() {
  process.stdout.write("\x1b[2J");
}

function resetStyle() {
  process.stdout.write("\x1b[0m");
}

function setColor(code) {
  process.stdout.write(`\x1b[${code}m`);
}

function setBold() {
  process.stdout.write("\x1b[1m");
}

function setRegular() {
  process.stdout.write("\x1b[21m");
}

export default class ScreenDevice {
  byteLength: number;
  buffer: ArrayBuffer;

  constructor() {
    this.byteLength = 0xff;
    this.buffer = new ArrayBuffer(0);
  }

  getUint16(_address: number): number {
    return 0;
  }

  getUint8(_address: number): number {
    return 0;
  }

  setUint16(address: number, data: number): void {
    const charCode = data & 0x00ff;
    const instruction = (data & 0xff00) >> 8;

    if (instruction === 0xff) {
      clearScreen();
    }

    if (instruction === 0x10) {
      setRegular();
    }

    if (instruction === 0x11) {
      setBold();
    }

    if (instruction === 0xfe) {
      resetStyle();
    }

    if (0x1e <= instruction && instruction <= 0x32) {
      setColor(instruction);
    }

    const x = (address % 16) + 1;
    const y = Math.floor(address / 16) + 1;

    moveTo(x, y);
    print(charCode);
  }

  setUint8(_address: number, _data: number): void {
    return;
  }
}
