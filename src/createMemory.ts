export interface Memory {
  getUint8: (address: number) => number;
  getUint16: (address: number) => number;
  setUint8: (address: number, byte: number) => void;
  setUint16: (address: number, bytes: number) => void;
  byteLength: number;
  buffer: ArrayBuffer;
}

export default function createMemory(size: number): Memory {
  const memory = new ArrayBuffer(size);
  const dataView = new DataView(memory);
  return dataView as Memory;
}
