import createMemory, { Memory } from "./createMemory";
import instructions from "./instructions/index";
import { registers } from "./registers";

export default class CPU {
  memory: Memory;
  _writeableBytes: Uint8Array;
  registerNames: string[];
  registers: Memory;
  registerMap: {
    [name: string]: number;
  };
  frameSize: number;

  constructor(memory: Memory) {
    this.memory = memory;
    this._writeableBytes = new Uint8Array(this.memory.buffer);
    this.registerNames = [];
    for (const register in registers) {
      this.registerNames.push(register);
    }

    this.registers = createMemory(this.registerNames.length * 2);
    this.registerMap = this.registerNames.reduce((map, registerName, index) => {
      map[registerName] = index * 2; // first register starts a byte 0, second at 2, third at 4 etc.
      return map;
    }, {});

    this.setRegister(registers.SP, this.memory.byteLength! - 1 - 1);
    this.setRegister(registers.FP, this.memory.byteLength! - 1 - 1);

    this.frameSize = 0;
  }

  load(machineCode: number[]): void {
    let i = 0;
    for (const byte of machineCode) {
      this._writeableBytes[i++] = byte;
    }
  }

  getRegister(register: number): number {
    if (register > this.registerNames.length) {
      throw new Error(
        `getRegister: Register 0x${register.toString(16)} not found.`
      );
    }

    return this.registers.getUint16(register * 2);
  }

  setRegister(register: number, value: number): void {
    if (register > this.registerNames.length) {
      throw new Error(
        `getRegister: Register 0x${register.toString(16)} not found.`
      );
    }
    this.registers.setUint16(register * 2, value);
  }

  fetch(): number {
    const currentPosition = this.getRegister(registers.IP);
    const currentValue = this.memory.getUint8(currentPosition);
    this.setRegister(registers.IP, currentPosition + 1);
    return currentValue;
  }

  fetch16(): number {
    const currentPosition = this.getRegister(registers.IP);
    const currentValue = this.memory.getUint16(currentPosition);
    this.setRegister(registers.IP, currentPosition + 2);
    return currentValue;
  }

  push(value: number) {
    const spAddress = this.getRegister(registers.SP);
    this.memory.setUint16(spAddress, value);
    this.setRegister(registers.SP, spAddress - 2);
    this.frameSize += 2;
  }

  pop(): number {
    const nextSpAddress = this.getRegister(registers.SP) + 2;
    this.setRegister(registers.SP, nextSpAddress);
    const value = this.memory.getUint16(nextSpAddress);
    this.frameSize -= 2;
    return value;
  }

  pushFrame(): void {
    this.push(this.getRegister(registers.R1));
    this.push(this.getRegister(registers.R2));
    this.push(this.getRegister(registers.R3));
    this.push(this.getRegister(registers.R4));
    this.push(this.getRegister(registers.R5));
    this.push(this.getRegister(registers.R6));
    this.push(this.getRegister(registers.R7));
    this.push(this.getRegister(registers.R8));
    this.push(this.getRegister(registers.IP));
    this.push(this.frameSize + 2);

    this.setRegister(registers.FP, this.getRegister(registers.SP));
    this.frameSize = 0;
  }

  popFrame(): void {
    const frameAddress = this.getRegister(registers.FP);
    this.setRegister(registers.SP, frameAddress);

    const frameSize = this.pop();
    this.frameSize = frameSize;

    this.setRegister(registers.IP, this.pop());
    this.setRegister(registers.R8, this.pop());
    this.setRegister(registers.R7, this.pop());
    this.setRegister(registers.R6, this.pop());
    this.setRegister(registers.R5, this.pop());
    this.setRegister(registers.R4, this.pop());
    this.setRegister(registers.R3, this.pop());
    this.setRegister(registers.R2, this.pop());
    this.setRegister(registers.R1, this.pop());

    const nArgs = this.pop();

    for (let i = 1; i <= nArgs; i++) {
      this.pop();
    }

    this.setRegister(registers.FP, frameAddress + frameSize);
  }

  debug(): void {
    console.log("\n");
    this.registerNames.forEach((registerName) => {
      const value = this.getRegister(registers[registerName])
        .toString(16)
        .padStart(4, "0");
      console.log(`${registerName}: 0x${value}`);
    });

    const ipPosition = this.getRegister(registers.IP);
    this.viewMemoryAt(ipPosition);
  }

  viewMemoryAt(address: number, numberOfBytes: number = 8): void {
    const nextBytes = Array(numberOfBytes)
      .fill(null)
      .map((_, index) => this.memory.getUint8(address + index));

    console.log(
      `0x${address.toString(16).padStart(4, "0")}: ${nextBytes
        .map((byte) => `0x${byte.toString(16).padStart(2, "0")}`)
        .join(" ")}`
    );
  }

  execute(instruction) {
    switch (instruction) {
      // move literal to Memory
      case instructions.MOV_LIT_MEM.opcode: {
        const literal = this.fetch16();
        const address = this.fetch16();
        this.memory.setUint16(address, literal);
        return;
      }

      // move literal to register
      case instructions.MOV_LIT_REG.opcode: {
        const literal = this.fetch16();
        const register = this.fetch();
        this.setRegister(register, literal);
        return;
      }

      // move register to register
      case instructions.MOV_REG_REG.opcode: {
        const registerFrom = this.fetch();
        const registerTo = this.fetch();

        const register1Value = this.getRegister(registerFrom);
        this.setRegister(registerTo, register1Value);
        return;
      }

      // move register to memory
      case instructions.MOV_REG_MEM.opcode: {
        const register = this.fetch();
        const memoryAddress = this.fetch16();

        const registerValue = this.getRegister(register);
        this.memory.setUint16(memoryAddress, registerValue);
        return;
      }

      // move memory to register
      case instructions.MOV_MEM_REG.opcode: {
        const memoryAddress = this.fetch16();
        const register = this.fetch();

        const memoryValue = this.memory.getUint16(memoryAddress);
        this.setRegister(register, memoryValue);
        return;
      }

      // move register pointer value to register
      case instructions.MOV_REG_PTR_REG.opcode: {
        const register1 = this.fetch();
        const register2 = this.fetch();
        const pointer = this.getRegister(register1);
        const value = this.memory.getUint16(pointer);
        this.setRegister(register2, value);
        return;
      }

      // move literal plus register value from memory address to register
      case instructions.MOV_LIT_OFF_REG.opcode: {
        const baseAddress = this.fetch16();
        const register1 = this.fetch();
        const register2 = this.fetch();

        const offset = this.getRegister(register1);
        const value = this.memory.getUint16(baseAddress + offset);

        this.setRegister(register2, value);
        return;
      }

      // add literal to register
      case instructions.ADD_LIT_REG.opcode: {
        const literal = this.fetch16();
        const register = this.fetch();

        const registerValue = this.getRegister(register);
        this.setRegister(registers.ACC, registerValue + literal);
        return;
      }

      // add register to register
      case instructions.ADD_REG_REG.opcode: {
        const register1 = this.fetch();
        const register2 = this.fetch();

        const registerValue1 = this.getRegister(register1);
        const registerValue2 = this.getRegister(register2);

        this.setRegister(registers.ACC, registerValue1 + registerValue2);
        return;
      }

      // subtract literal from register
      case instructions.SUB_LIT_REG.opcode: {
        const literal = this.fetch16();
        const register = this.fetch();

        const registerValue = this.getRegister(register);
        this.setRegister(registers.ACC, registerValue - literal);
        return;
      }

      // subtract literal from register
      case instructions.SUB_REG_LIT.opcode: {
        const register = this.fetch();
        const literal = this.fetch16();

        const registerValue = this.getRegister(register);
        this.setRegister(registers.ACC, literal - registerValue);
        return;
      }

      // subtract register from register
      case instructions.SUB_REG_REG.opcode: {
        const register1 = this.fetch();
        const register2 = this.fetch();

        const registerValue1 = this.getRegister(register1);
        const registerValue2 = this.getRegister(register2);

        this.setRegister(registers.ACC, registerValue2 - registerValue1);
        return;
      }

      // multiply literal with register
      case instructions.MUL_LIT_REG.opcode: {
        const literal = this.fetch16();
        const register = this.fetch();

        const registerValue = this.getRegister(register);
        this.setRegister(registers.ACC, registerValue * literal);
        return;
      }

      // multiply register with register
      case instructions.MUL_REG_REG.opcode: {
        const register1 = this.fetch();
        const register2 = this.fetch();

        const registerValue1 = this.getRegister(register1);
        const registerValue2 = this.getRegister(register2);

        this.setRegister(registers.ACC, registerValue1 * registerValue2);
        return;
      }

      // increment register
      case instructions.INC_REG.opcode: {
        const register = this.fetch();
        const value = this.getRegister(register);
        this.setRegister(register, value + 1);
        return;
      }

      // decrement register
      case instructions.DEC_REG.opcode: {
        const register = this.fetch();
        const value = this.getRegister(register);
        this.setRegister(register, value - 1);
        return;
      }

      // left shift register by literal
      case instructions.LSF_REG_LIT.opcode: {
        const register = this.fetch();
        const literal = this.fetch16();

        const registerValue = this.getRegister(register);

        this.setRegister(registers.ACC, registerValue << literal);
        return;
      }

      // left shift register by register
      case instructions.LSF_REG_REG.opcode: {
        const register1 = this.fetch();
        const register2 = this.fetch();

        const registerValue1 = this.getRegister(register1);
        const registerValue2 = this.getRegister(register2);

        this.setRegister(registers.ACC, registerValue1 << registerValue2);
        return;
      }

      // right shift register by literal
      case instructions.RSF_REG_LIT.opcode: {
        const register = this.fetch();
        const literal = this.fetch16();

        const registerValue = this.getRegister(register);

        this.setRegister(registers.ACC, registerValue >> literal);
        return;
      }

      // right shift register by register
      case instructions.RSF_REG_REG.opcode: {
        const register1 = this.fetch();
        const register2 = this.fetch();

        const registerValue1 = this.getRegister(register1);
        const registerValue2 = this.getRegister(register2);

        this.setRegister(registers.ACC, registerValue1 >> registerValue2);
        return;
      }

      // and register by literal
      case instructions.AND_REG_LIT.opcode: {
        const register = this.fetch();
        const literal = this.fetch16();

        const registerValue = this.getRegister(register);

        this.setRegister(registers.ACC, registerValue & literal);
        return;
      }

      // and register by register
      case instructions.AND_REG_REG.opcode: {
        const register1 = this.fetch();
        const register2 = this.fetch();

        const registerValue1 = this.getRegister(register1);
        const registerValue2 = this.getRegister(register2);

        this.setRegister(registers.ACC, registerValue1 & registerValue2);
        return;
      }

      // or register by literal
      case instructions.OR_REG_LIT.opcode: {
        const register = this.fetch();
        const literal = this.fetch16();

        const registerValue = this.getRegister(register);

        this.setRegister(registers.ACC, registerValue | literal);
        return;
      }

      // or register by register
      case instructions.OR_REG_REG.opcode: {
        const register1 = this.fetch();
        const register2 = this.fetch();

        const registerValue1 = this.getRegister(register1);
        const registerValue2 = this.getRegister(register2);

        this.setRegister(registers.ACC, registerValue1 | registerValue2);
        return;
      }

      // xor register by literal
      case instructions.XOR_REG_LIT.opcode: {
        const register = this.fetch();
        const literal = this.fetch16();

        const registerValue = this.getRegister(register);

        this.setRegister(registers.ACC, registerValue ^ literal);
        return;
      }

      // xor register by register
      case instructions.XOR_REG_REG.opcode: {
        const register1 = this.fetch();
        const register2 = this.fetch();

        const registerValue1 = this.getRegister(register1);
        const registerValue2 = this.getRegister(register2);

        this.setRegister(registers.ACC, registerValue1 ^ registerValue2);
        return;
      }

      // not a register
      case instructions.NOT.opcode: {
        const register = this.fetch();
        const value = this.getRegister(register);
        this.setRegister(registers.ACC, ~value & 0x0000ffff);
        return;
      }

      // jump to address if acc not equal to literal
      case instructions.JNE_LIT.opcode: {
        const value = this.fetch16();
        const jumpToAddress = this.fetch16();

        if (value !== this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if acc not equal to register
      case instructions.JNE_REG.opcode: {
        const register = this.fetch();
        const jumpToAddress = this.fetch16();

        const value = this.getRegister(register);

        if (value !== this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if literal is equal to acc
      case instructions.JEQ_LIT.opcode: {
        const value = this.fetch16();
        const jumpToAddress = this.fetch16();

        if (value === this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if register is equal to acc
      case instructions.JEQ_REG.opcode: {
        const register = this.fetch();
        const jumpToAddress = this.fetch16();

        const value = this.getRegister(register);

        if (value === this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if literal is less than acc
      case instructions.JLT_LIT.opcode: {
        const value = this.fetch16();
        const jumpToAddress = this.fetch16();

        if (value < this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if register is less than acc
      case instructions.JLT_REG.opcode: {
        const register = this.fetch();
        const jumpToAddress = this.fetch16();

        const value = this.getRegister(register);

        if (value < this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if literal is greater than acc
      case instructions.JGT_LIT.opcode: {
        const value = this.fetch16();
        const jumpToAddress = this.fetch16();

        if (value > this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if register is greater than acc
      case instructions.JGT_REG.opcode: {
        const register = this.fetch();
        const jumpToAddress = this.fetch16();

        const value = this.getRegister(register);

        if (value > this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if literal is less or equal to acc
      case instructions.JLE_LIT.opcode: {
        const value = this.fetch16();
        const jumpToAddress = this.fetch16();

        if (value <= this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if register is less or equal to acc
      case instructions.JLE_REG.opcode: {
        const register = this.fetch();
        const jumpToAddress = this.fetch16();

        const value = this.getRegister(register);

        if (value <= this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if literal is greater or equal to acc
      case instructions.JGE_LIT.opcode: {
        const value = this.fetch16();
        const jumpToAddress = this.fetch16();

        if (value >= this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      // jump to address if register is greater or equal to acc
      case instructions.JGE_REG.opcode: {
        const register = this.fetch();
        const jumpToAddress = this.fetch16();

        const value = this.getRegister(register);

        if (value >= this.getRegister(registers.ACC)) {
          this.setRegister(registers.IP, jumpToAddress);
        }
        return;
      }

      //push literal onto stack
      case instructions.PSH_LIT.opcode: {
        const literal = this.fetch16();
        this.push(literal);
        return;
      }

      //push register onto stack
      case instructions.PSH_REG.opcode: {
        const register = this.fetch();
        const registerValue = this.getRegister(register);
        this.push(registerValue);
        return;
      }

      //pop stack into register
      case instructions.POP.opcode: {
        const register = this.fetch();
        const value = this.pop();

        this.setRegister(register, value);
        return;
      }

      case instructions.CAL_LIT.opcode: {
        const address = this.fetch16();
        this.pushFrame();
        this.setRegister(registers.IP, address);
        return;
      }

      case instructions.CAL_REG.opcode: {
        const register = this.fetch();
        const address = this.getRegister(register);
        this.pushFrame();
        this.setRegister(registers.IP, address);
        return;
      }

      case instructions.RET.opcode: {
        this.popFrame();
        return;
      }

      // Halt run function
      case instructions.HLT.opcode: {
        return true;
      }

      case instructions.NOP.opcode: {
        return;
      }

      default:
        throw new Error(`execute: unknown instruction ${instruction}.`);
    }
  }

  step() {
    const instruction = this.fetch();
    return this.execute(instruction);
  }

  run() {
    const halt = this.step();

    if (!halt) {
      setTimeout(() => this.run(), 0);
    }
  }

  runWithDebug() {
    const halt = this.step();
    this.debug();

    if (!halt) {
      setImmediate(() => this.runWithDebug());
    }
  }
}
