import * as A from "arcsecond";
import {
  litMem,
  litOffReg,
  litReg,
  memReg,
  noArgs,
  regLit,
  regMem,
  regPtrReg,
  regReg,
  singleLit,
  singleReg,
} from "./formats";

const mov = A.choice([
  litMem("mov", "MOV_LIT_MEM"),
  litReg("mov", "MOV_LIT_REG"),
  regReg("mov", "MOV_REG_REG"),
  memReg("mov", "MOV_MEM_REG"),
  regMem("mov", "MOV_REG_MEM"),
  regPtrReg("mov", "MOV_REG_PTR_REG"),
  litOffReg("mov", "MOV_LIT_OFF_REG"),
]);

const add = A.choice([
  regReg("add", "ADD_REG_REG"),
  litReg("add", "ADD_LIT_REG"),
]);

const sub = A.choice([
  regReg("sub", "SUB_REG_REG"),
  litReg("sub", "SUB_LIT_REG"),
  regLit("sub", "SUB_REG_LIT"),
]);

const mul = A.choice([
  regReg("mul", "MUL_REG_REG"),
  litReg("mul", "MUL_LIT_REG"),
]);

const inc = singleReg("inc", "INC_REG");
const dec = singleReg("dec", "DEC_REG");

const lsf = A.choice([
  regReg("lsf", "LSF_REG_REG"),
  regLit("lsf", "LSF_REG_LIT"),
]);

const rsf = A.choice([
  regReg("rsf", "RSF_REG_REG"),
  regLit("rsf", "RSF_REG_LIT"),
]);

const and = A.choice([
  regReg("and", "AND_REG_REG"),
  regLit("and", "AND_REG_LIT"),
]);

const or = A.choice([regReg("or", "OR_REG_REG"), litReg("or", "OR_REG_LIT")]);

const xor = A.choice([
  regReg("xor", "XOR_REG_REG"),
  regLit("xor", "XOR_REG_LIT"),
]);

const not = singleReg("not", "NOT");

const jne = A.choice([litMem("jne", "JNE_LIT"), regMem("jne", "JNE_REG")]);

const jeq = A.choice([litMem("jeq", "JEQ_LIT"), regMem("jeq", "JEQ_REG")]);

const jlt = A.choice([litMem("jlt", "JLT_LIT"), regMem("jlt", "JLT_REG")]);

const jgt = A.choice([litMem("jgt", "JGT_LIT"), regMem("jgt", "JGT_REG")]);

const jle = A.choice([litMem("jle", "JLE_LIT"), regMem("jle", "JLE_REG")]);

const jge = A.choice([litMem("jge", "JGE_LIT"), regMem("jge", "JGE_REG")]);

const psh = A.choice([
  singleLit("psh", "PSH_LIT"),
  singleReg("psh", "PSH_REG"),
]);

const pop = singleReg("pop", "POP");

const cal = A.choice([
  singleLit("cal", "CAL_LIT"),
  singleReg("cal", "CAL_REG"),
]);

const ret = noArgs("ret", "RET");
const hlt = noArgs("hlt", "HLT");

const instruction = A.choice([
  mov,
  add,
  sub,
  mul,
  inc,
  dec,
  lsf,
  rsf,
  and,
  or,
  xor,
  not,
  jne,
  jeq,
  jlt,
  jgt,
  jle,
  jge,
  psh,
  pop,
  cal,
  ret,
  hlt,
]);

export default instruction;
