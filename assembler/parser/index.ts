import instruction from "./instruction";
import * as A from "arcsecond";
import { label } from "./primitives";
import { ParsedValue } from "./types";

export default A.many<ParsedValue>(A.choice([label, instruction]));
