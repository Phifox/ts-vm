import * as A from "arcsecond";
import { ParsedValue, asType } from "./types";
import { peek } from "./utils";
import { bracketedExpr } from "./bracketedExpression";
import { hexLiteral, operator, variable } from "./primitives";
import { flow } from "./stateflow";

function disambiguateOrderOfOperations(expr: ParsedValue) {
  if (
    expr.type !== "SQUARE_BRACKET_EXPRESSION" &&
    expr.type !== "BRACKETED_EXPRESSION"
  ) {
    return expr;
  }

  if (!Array.isArray(expr.value)) {
    throw new Error("Error: unexpected value (disambiguateOrderOfOperations)");
  }

  if (expr.value.length === 1) {
    return expr.value[0];
  }

  const priorities = {
    OP_MULTIPLY: 2,
    OP_PLUS: 1,
    OP_MINUS: 0,
  };

  let candidateExpression = {
    op: "error",
    priority: -Infinity,
    a: 0,
    b: 0,
  };

  for (let i = 1; i < expr.value.length; i += 2) {
    const element = expr.value[i];
    const level = priorities[element.type];
    if (level > candidateExpression.priority) {
      candidateExpression = {
        op: element.type,
        priority: level,
        a: i - 1,
        b: i + 1,
      };
    }
  }

  const disambiguatedExpression = asType("BINARY_OPERATION")({
    a: disambiguateOrderOfOperations(expr.value[candidateExpression.a]),
    b: disambiguateOrderOfOperations(expr.value[candidateExpression.b]),
    op: candidateExpression.op,
  });

  const newExpression = asType("BRACKETED_EXPRESSION")([
    ...expr.value.slice(0, candidateExpression.a),
    disambiguatedExpression,
    ...expr.value.slice(candidateExpression.b + 1),
  ]);

  return disambiguateOrderOfOperations(newExpression);
}

export const squareBracketExpr = A.coroutine((run) => {
  run(A.char("["));
  run(A.optionalWhitespace);

  const expr: ParsedValue[] = [];

  flow({
    START() {
      return this.EXPECT_ELEMENT;
    },

    EXPECT_ELEMENT() {
      const result = run(A.choice([bracketedExpr, hexLiteral, variable]));

      if (!result) {
        throw new Error(
          "Error: bracketedExpr returned undefined (squareBracketedExpr)"
        );
      }
      expr.push(result);

      run(A.optionalWhitespace);

      return this.EXPECT_OPERATOR;
    },

    EXPECT_OPERATOR() {
      const nextChar = run(peek);
      if (nextChar === "]") {
        run(A.char("]"));
        run(A.optionalWhitespace);

        return this.DONE;
      }

      const result = run(operator);
      expr.push(result);

      run(A.optionalWhitespace);

      return this.EXPECT_ELEMENT;
    },
  });

  return asType("SQUARE_BRACKET_EXPRESSION")(expr);
}).map(disambiguateOrderOfOperations);
