export interface BracketedExpression
  extends Array<ParsedValue | BracketedExpression> {}

export interface InstructionValue {
  instruction: string;
  args: ParsedValue[];
}
export interface BinaryOperationValue {
  a: ParsedValue;
  b: ParsedValue;
  op: string;
}

type Value = string | InstructionValue | ParsedValue[] | BinaryOperationValue;

export interface ParsedValue {
  type: string;
  value: Value;
}

type AsType = (type: string) => (value: Value) => ParsedValue;

export const asType: AsType = (type) => (value) => ({ type, value });
