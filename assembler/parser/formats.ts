import * as A from "arcsecond";
import { address, hexLiteral, register } from "./primitives";
import { asType } from "./types";
import { upperOrLowerStr } from "./primitives";
import { squareBracketExpr } from "./squareBracketExpression";

export const litReg = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.whitespace);

    const literal = run(A.choice([hexLiteral, squareBracketExpr]));

    run(A.optionalWhitespace);
    run(A.char(","));
    run(A.optionalWhitespace);

    const register1 = run(register);
    run(A.optionalWhitespace);

    return asType("INSTRUCTION")({
      instruction: type,
      args: [literal, register1],
    });
  });

export const regLit = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.whitespace);

    const register1 = run(register);

    run(A.optionalWhitespace);
    run(A.char(","));
    run(A.optionalWhitespace);

    const literal = run(A.choice([hexLiteral, squareBracketExpr]));

    run(A.optionalWhitespace);

    return asType("INSTRUCTION")({
      instruction: type,
      args: [register1, literal],
    });
  });

export const regReg = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.whitespace);

    const register1 = run(register);

    run(A.optionalWhitespace);
    run(A.char(","));
    run(A.optionalWhitespace);

    const register2 = run(register);
    run(A.optionalWhitespace);

    return asType("INSTRUCTION")({
      instruction: type,
      args: [register1, register2],
    });
  });

export const regMem = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.whitespace);

    const register1 = run(register);

    run(A.optionalWhitespace);
    run(A.char(","));
    run(A.optionalWhitespace);

    const address1 = run(
      A.choice([address, A.char("&").chain(() => squareBracketExpr)])
    );
    run(A.optionalWhitespace);

    return asType("INSTRUCTION")({
      instruction: type,
      args: [register1, address1],
    });
  });

export const memReg = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.whitespace);

    const address1 = run(
      A.choice([address, A.char("&").chain(() => squareBracketExpr)])
    );

    run(A.optionalWhitespace);
    run(A.char(","));
    run(A.optionalWhitespace);

    const register1 = run(register);
    run(A.optionalWhitespace);

    return asType("INSTRUCTION")({
      instruction: type,
      args: [address1, register1],
    });
  });

export const litMem = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.whitespace);

    const literal = run(A.choice([hexLiteral, squareBracketExpr]));

    run(A.optionalWhitespace);
    run(A.char(","));
    run(A.optionalWhitespace);

    const address1 = run(
      A.choice([address, A.char("&").chain(() => squareBracketExpr)])
    );
    run(A.optionalWhitespace);

    return asType("INSTRUCTION")({
      instruction: type,
      args: [literal, address1],
    });
  });

export const regPtrReg = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.whitespace);

    const register1 = run(A.char("&").chain(() => register));

    run(A.optionalWhitespace);
    run(A.char(","));
    run(A.optionalWhitespace);

    const register2 = run(register);
    run(A.optionalWhitespace);

    return asType("INSTRUCTION")({
      instruction: type,
      args: [register1, register2],
    });
  });

export const litOffReg = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.whitespace);

    const literal = run(A.choice([hexLiteral, squareBracketExpr]));

    run(A.optionalWhitespace);
    run(A.char(","));
    run(A.optionalWhitespace);

    const register1 = run(A.char("&").chain(() => register));

    run(A.optionalWhitespace);
    run(A.char(","));
    run(A.optionalWhitespace);

    const register2 = run(register);
    run(A.optionalWhitespace);

    return asType("INSTRUCTION")({
      instruction: type,
      args: [literal, register1, register2],
    });
  });

export const noArgs = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.optionalWhitespace);

    return asType("INSTRUCTION")({
      instruction: type,
      args: [],
    });
  });

export const singleReg = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.optionalWhitespace);

    const register1 = run(register);
    run(A.optionalWhitespace);

    return asType("INSTRUCTION")({
      instruction: type,
      args: [register1],
    });
  });

export const singleLit = (mnemonic, type) =>
  A.coroutine((run) => {
    run(upperOrLowerStr(mnemonic));
    run(A.optionalWhitespace);

    const literal = run(A.choice([hexLiteral, squareBracketExpr]));

    return asType("INSTRUCTION")({
      instruction: type,
      args: [literal],
    });
  });
