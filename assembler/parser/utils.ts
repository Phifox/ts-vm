import * as A from "arcsecond";
import { Parser } from "arcsecond";
import { inspect } from "util";

export function deepLog(x: any) {
  console.log(
    inspect(x, {
      depth: Infinity,
      colors: true,
    })
  );
}

export function mapJoin(parser: Parser<any>): Parser<any> {
  return parser.map((elements) => elements.join(""));
}

export const peek = A.lookAhead(A.regex(/^./));
