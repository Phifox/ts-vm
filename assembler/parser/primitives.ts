import * as A from "arcsecond";
import { asType } from "./types";
import { mapJoin } from "./utils";

export const upperOrLowerStr = (string: string): A.Parser<any> =>
  A.choice([A.str(string.toUpperCase()), A.str(string.toLowerCase())]);

export const register = A.choice([
  upperOrLowerStr("ip"),
  upperOrLowerStr("acc"),
  upperOrLowerStr("r1"),
  upperOrLowerStr("r2"),
  upperOrLowerStr("r3"),
  upperOrLowerStr("r4"),
  upperOrLowerStr("r5"),
  upperOrLowerStr("r6"),
  upperOrLowerStr("r7"),
  upperOrLowerStr("r8"),
  upperOrLowerStr("sp"),
  upperOrLowerStr("fp"),
]).map(asType("REGISTER"));

const hexDigit = A.regex(/^[0-9A-Fa-f]/);

const validIdentifier = mapJoin(
  A.sequenceOf([
    A.regex(/^[a-zA-Z_]/),
    A.possibly(A.regex(/^[a-zA-Z0-9_]+/)).map((item) =>
      item === null ? "" : item
    ),
  ])
);

export const label = A.sequenceOf([
  validIdentifier,
  A.char(":"),
  A.optionalWhitespace,
])
  .map(([labelName]) => labelName)
  .map(asType("LABEL"));

export const variable = A.char("!")
  .chain(() => validIdentifier)
  .map(asType("VARIABLE"));

export const hexLiteral = A.char("$").chain(() =>
  mapJoin(A.many1(hexDigit)).map(asType("HEX_LITERAL"))
);

export const address = A.char("&").chain(() =>
  mapJoin(A.many1(hexDigit)).map(asType("ADDRESS"))
);

export const operator = A.choice([
  A.char("+").map(asType("OP_PLUS")),
  A.char("-").map(asType("OP_MINUS")),
  A.char("*").map(asType("OP_MULTIPLY")),
]);
