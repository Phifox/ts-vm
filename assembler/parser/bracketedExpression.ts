import * as A from "arcsecond";
import { hexLiteral, operator, variable } from "./primitives";
import { BracketedExpression, ParsedValue, asType } from "./types";
import { peek } from "./utils";
import { flow } from "./stateflow";

function typifyBracketedExpression(expr: BracketedExpression): ParsedValue {
  const asBracketed = asType("BRACKETED_EXPRESSION");

  const typedExpression = expr.map(
    (element: ParsedValue | BracketedExpression) => {
      if (Array.isArray(element)) {
        return typifyBracketedExpression(element);
      }

      return element;
    }
  );

  return asBracketed(typedExpression);
}

export const bracketedExpr = A.coroutine((run) => {
  const stack: BracketedExpression[] = [[]];
  const expr: BracketedExpression = stack.at(-1)!;
  run(A.char("("));
  run(A.optionalWhitespace);

  flow({
    START() {
      return this.ELEMENT_OR_OPENING_BRACKET;
    },

    OPEN_BRACKET() {
      run(A.char("("));
      run(A.optionalWhitespace);

      const nestedExpr: ParsedValue[] = [];
      stack.push(nestedExpr);
      expr.push(nestedExpr);

      return this.ELEMENT_OR_OPENING_BRACKET;
    },

    ELEMENT_OR_OPENING_BRACKET() {
      const nextChar = run(peek);
      if (nextChar === "(") {
        return this.OPEN_BRACKET;
      }
      const value = run(A.choice([hexLiteral, variable]));
      stack.at(-1)!.push(value);

      run(A.optionalWhitespace);
      return this.OPERATOR_OR_CLOSING_BRACKET;
    },

    OPERATOR_OR_CLOSING_BRACKET() {
      const nextChar = run(peek);
      if (nextChar === ")") {
        return this.CLOSE_BRACKET;
      }

      const value = run(operator);
      stack.at(-1)!.push(value);

      run(A.optionalWhitespace);
      return this.ELEMENT_OR_OPENING_BRACKET;
    },

    CLOSE_BRACKET() {
      run(A.char(")"));
      run(A.optionalWhitespace);

      stack.pop();

      if (stack.length === 0) {
        return this.DONE;
      }

      return this.OPERATOR_OR_CLOSING_BRACKET;
    },
  });

  return typifyBracketedExpression(expr);
});
