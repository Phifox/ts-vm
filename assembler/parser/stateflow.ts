type stateFunction = () => stateFunction | symbol;

interface States {
  START: stateFunction;
  [state: string]: stateFunction | symbol;
}

export function flow(states: States) {
  states.DONE = Symbol("done");
  states._current = states.START;

  while (states._current !== states.DONE) {
    states._current = (states._current as stateFunction)();
  }
}
