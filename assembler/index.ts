import parser from "./parser/index";
import { registers } from "../src/registers";
import { InstructionValue, ParsedValue } from "./parser/types";
import instructions from "../src/instructions/index";

function encoderFactory(labels) {
  const machineCode: number[] = [];

  const checkForVariable = (token: ParsedValue) => {
    if (token.type !== "VARIABLE") {
      return parseInt(token.value as string, 16);
    }

    if (labels[token.value as string] === undefined) {
      throw new Error(`Error: ${token.value} could not be resolved.`);
    }

    return labels[token.value as string];
  };

  const encodeLitOrMem = (litOrMem: ParsedValue) => {
    let hexVal = checkForVariable(litOrMem);

    const highByte = (hexVal & 0xff00) >> 8;
    const lowByte = hexVal & 0x00ff;
    machineCode.push(highByte, lowByte);
  };

  const encodeLit8 = (lit: ParsedValue) => {
    const hexVal = checkForVariable(lit);
    const lowByte = hexVal & 0x00ff;
    machineCode.push(lowByte);
  };

  const encodeReg = (reg: ParsedValue) => {
    const register = registers[(reg.value as string).toUpperCase()];
    machineCode.push(register);
  };
  return {
    output: machineCode,
    instruction(opcode) {
      machineCode.push(opcode);
    },
    litReg(args) {
      encodeLitOrMem(args[0]);
      encodeReg(args[1]);
    },
    regLit(args) {
      encodeReg(args[0]);
      encodeLitOrMem(args[1]);
    },
    regLit8(args) {
      encodeReg(args[0]);
      encodeLit8(args[1]);
    },
    regReg(args) {
      encodeReg(args[0]);
      encodeReg(args[1]);
    },
    regMem(args) {
      encodeReg(args[0]);
      encodeLitOrMem(args[1]);
    },
    memReg(args) {
      encodeLitOrMem(args[0]);
      encodeReg(args[1]);
    },
    litMem(args) {
      encodeLitOrMem(args[0]);
      encodeLitOrMem(args[1]);
    },
    regPtrReg(args) {
      encodeReg(args[0]);
      encodeReg(args[1]);
    },
    litOffReg(args) {
      encodeLitOrMem(args[0]);
      encodeReg(args[1]);
      encodeReg(args[2]);
    },
    singleReg(args) {
      encodeReg(args[0]);
    },
    singleLit(args) {
      encodeLitOrMem(args[0]);
    },
    noArgs() {},
  };
}

interface ParsedOutput {
  result: ParsedValue[];
}

interface Labels {
  [label: string]: number;
}

function hoistLabels(parsedCode: ParsedValue[]): Labels {
  const labels = {};
  let currentAddress = 0x00;

  parsedCode.forEach((item) => {
    if (item.type === "INSTRUCTION") {
      const metaData =
        instructions[(item.value as InstructionValue).instruction];
      currentAddress += metaData.size;
    }

    if (item.type === "LABEL") {
      labels[item.value as string] = currentAddress;
    }
  });

  return labels;
}

export default function compile(program) {
  const sanitizedProgram = program.trim();
  const { result: parsedOutput } = parser.run(sanitizedProgram) as ParsedOutput;

  const labels = hoistLabels(parsedOutput);

  console.log(labels);

  const encoder = encoderFactory(labels);
  parsedOutput
    .filter((item) => item.type === "INSTRUCTION")
    .forEach((instruction) => {
      const value = instruction.value as InstructionValue;
      const metaData = instructions[value.instruction];
      encoder.instruction(metaData.opcode);
      encoder[metaData.type](value.args);
    });
  console.log(encoder.output);
  return encoder.output;
}
